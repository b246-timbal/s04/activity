from abc import ABC, abstractclassmethod

# Base Class
class Animal(ABC):
   @abstractclassmethod
   def eat(self, food):
       pass
   
   def make_sound(self):
       pass

# Derived Class
class Cat(Animal):

    def __init__(self, name, breed, age):
        super().__init__()
        self._name = name
        self._breed = breed
        self._age = age

    def set_name(self, name):
        self._name = name

    def set_breed(self, breed):
        self._breed = breed

    def set_age(self, age):
        self._age = age
    
    def get_name(self):
        print(f"Hi my name is {self._name}")

    def get_breed(self):
        print(f"I am {self._breed}")

    def get_age(self):
        print(f"I am {self._age} years old")
    

    def make_sound(self):
        print(f"Miaow! Nyaw! Nyaaaaa!")

    def eat(self, food):
        self._food = food
        print(f"Serve me {self._food}")

    def call(self):
        print(f"{self._name}, come on!")

# Derived Class
class Dog(Animal):

    def __init__(self, name, breed, age):
        super().__init__()
        self._name = name
        self._breed = breed
        self._age = age

    def set_name(self, name):
        self._name = name

    def set_breed(self, breed):
        self._breed = breed

    def set_age(self, age):
        self._age = age

    def get_name(self):
        print(f"Hi my name is {self._name}")

    def get_breed(self):
        print(f"I am {self._breed}")

    def get_age(self):
        print(f"I am {self._age} years old")
    

    def make_sound(self):
        print(f"Bark! Woof! Arf!")

    def eat(self, food):
        self._food = food
        print(f"Serve me {self._food}")

    def call(self):
        print(f"Here {self._name}!")

    

# Test Cases:
dog1 = Dog("Isis", "Dalmatian", 15)
dog1.eat("Steak")
dog1.make_sound()
dog1.call()

cat1 = Cat("Puss", "Persian", 4)
cat1.eat("Tuna")
cat1.make_sound()
cat1.call()

